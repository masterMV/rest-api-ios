//
//  User.swift
//  restLab
//
//  Created by maxim vingalov on 30.05.2018.
//  Copyright © 2018 maxim vingalov. All rights reserved.
//

import UIKit
import ObjectMapper

public class User: NSObject, Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let id = "id"
        static let phone = "phone"
        static let password = "password"
    }
    
    // MARK: Properties
    public var id: String?
    public var phone: String?
    public var password: String?
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public required init?(map: Map) {
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        id <- map[SerializationKeys.id]
        phone <- map[SerializationKeys.phone]
        password <- map[SerializationKeys.password]
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = phone { dictionary[SerializationKeys.phone] = value }
        if let value = password { dictionary[SerializationKeys.password] = value }
        return dictionary
    }
    
    init(phone: String, password: String) {
        self.phone = phone
        self.password = password
    }
    
}

