//
//  BaseView.swift
//  restLab
//
//  Created by maxim vingalov on 30.05.2018.
//  Copyright © 2018 maxim vingalov. All rights reserved.
//

import UIKit

class BaseView: UIView {
    
    fileprivate (set) var topLayoutGuide: CGFloat = 0
    fileprivate (set) var bottomLayoutGuide: CGFloat = 0
    
    fileprivate weak var controller: UIViewController?
    
    init(controller: UIViewController) {
        super.init(frame: UIScreen.main.bounds)
        self.controller = controller
        backgroundColor = .white
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setLayoutGuides(top: CGFloat, bottom: CGFloat) {
        var didChange = false
        
        print("top layout guide \(top)")
        if top != topLayoutGuide {
            topLayoutGuide = top
            didChange = true
        }
        
        if bottom != bottomLayoutGuide {
            bottomLayoutGuide = bottom
            didChange = true
        }
        
        if didChange {
            didChangeLayoutGuides()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        topLayoutGuide = controller?.topLayoutGuide.length ?? 0
        bottomLayoutGuide = controller?.bottomLayoutGuide.length ?? 0
        
    }
    
    
    func didChangeLayoutGuides() {
        setNeedsLayout()
    }
    
    
}

