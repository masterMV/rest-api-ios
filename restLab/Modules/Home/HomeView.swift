//
//  HomeView.swift
//  restLab
//
//  Created by maxim vingalov on 31.05.2018.
//  Copyright © 2018 maxim vingalov. All rights reserved.
//

import UIKit

class HomeView: BaseView {

    weak var controller: HomeVC!

    lazy var refreshControl: UIRefreshControl = {
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(controller, action: #selector(HomeVC.refreshData), for: .valueChanged)

        return refreshControl
        
    }()
    
    lazy var searchBar: UISearchBar = {
        
        let searchBar = UISearchBar()
        searchBar.delegate = controller
        searchBar.showsCancelButton = true
        searchBar.showsBookmarkButton = false
        searchBar.showsSearchResultsButton = false
        searchBar.placeholder = "Введите текст..."
        
        return searchBar
        
    }()

    
    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.backgroundColor = .white
        tableView.dataSource = controller
        tableView.delegate = controller
        tableView.allowsSelection = true
        tableView.estimatedRowHeight = 60
        
        return tableView
    }()

    init(controller: HomeVC) {
        super.init(controller: controller)
        self.controller = controller
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        addSubview(searchBar)
        addSubview(tableView)
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layoutUI()
    }
    
    fileprivate func layoutUI() {
        
        searchBar.pin.top(topLayoutGuide).left().right().height(48)
        tableView.pin.below(of: searchBar).left().bottomRight()
        
    }


}
