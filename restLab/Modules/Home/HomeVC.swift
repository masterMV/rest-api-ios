//
//  HomeVC.swift
//  restLab
//
//  Created by maxim vingalov on 30.05.2018.
//  Copyright © 2018 maxim vingalov. All rights reserved.
//

import UIKit

class HomeVC: BaseVC {

    fileprivate var presenter: HomePresenter!
    
    fileprivate var mainView: HomeView { return self.view as! HomeView }
    
    override func loadView() {
        self.view = HomeView(controller: self)
    }
    
    init(with presenter: HomePresenter) {
        super.init()
        self.presenter = presenter
        self.presenter.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Главная"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "админ", style: .plain, target: self, action: #selector(showAdminVC))

        NotificationCenter.default.addObserver(self, selector: #selector(boardsStateChanged), name: Notification.Name(Constant.BOARDS_STATE_CHANGED), object: nil)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    @objc func showAdminVC() {
        
        let adminHomePresenter = AdminHomePresenter()
        let adminHomeVC = AdminHomeVC(with: adminHomePresenter)
        
        self.navigationController?.pushViewController(adminHomeVC, animated: true)
        
    }
    
    @objc func refreshData() {
        
        presenter.loadBoards()
        
    }
    
    @objc func boardsStateChanged() {
        presenter.loadBoards()
    }

    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

}

extension HomeVC: HomeProtocol {

    func boardsReceived(boards: [Board]) {
        mainView.refreshControl.endRefreshing()
        mainView.tableView.reloadData()
    }
    
}

extension HomeVC: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        presenter.searchBoards(searchString: searchBar.text ?? "")
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        mainView.endEditing(true)
        mainView.searchBar.text = ""
        presenter.loadBoards()
        
    }
    
    
}


extension HomeVC: UITableViewDataSource, UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.boards?.count ?? 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "Cell"

        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        
        if cell == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: cellIdentifier)
        }
        
        guard let boards = presenter.boards else { return cell! }
        
        let board = boards[indexPath.row]
        
        cell?.textLabel?.numberOfLines = 0
        cell?.textLabel?.text = board.title
        cell?.detailTextLabel?.text = board.desc
        cell?.accessoryType = .disclosureIndicator
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let boards = presenter.boards else { return }

        let board = boards[indexPath.row]

        let detailsPresenter = DetailsPresenter(with: board)
        let detailsVC = DetailsVC(with: detailsPresenter)

        self.navigationController?.pushViewController(detailsVC, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Доски объявлений"
    }
    
}


