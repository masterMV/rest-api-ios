//
//  HomePresenter.swift
//  restLab
//
//  Created by maxim vingalov on 31.05.2018.
//  Copyright © 2018 maxim vingalov. All rights reserved.
//

import UIKit

protocol HomeProtocol: AnyObject {
    func boardsReceived(boards: [Board])
}


class HomePresenter: NSObject {

    weak var delegate: HomeProtocol?
    
    var boards: [Board]?
    
    override init() {
        super.init()
        
        loadBoards()
        
    }
    
    func loadBoards() {
        
        ServerManager.shared.getBoards { [weak self] (json) in
            
            self?.boards = [Board]()
            
            for result in json["boards"].arrayValue {
                if let boardObj = result.dictionaryObject {
                    if let board = Board(JSON: boardObj) {
                        self?.boards?.append(board)
                    }
                }
            }
            
            if let boards = self?.boards {
                self?.delegate?.boardsReceived(boards: boards)
            }
            
        }

    }
    
    func searchBoards(searchString: String) {
        
        ServerManager.shared.searchBoard(searchString: searchString) { [weak self] (json) in
            
            self?.boards = [Board]()
            
            for result in json["boards"].arrayValue {
                if let boardObj = result.dictionaryObject {
                    if let board = Board(JSON: boardObj) {
                        self?.boards?.append(board)
                    }
                }
            }
            
            if let boards = self?.boards {
                self?.delegate?.boardsReceived(boards: boards)
            }
            
        }
        
    }

    
    
}
