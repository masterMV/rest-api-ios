//
//  AuthPresenter.swift
//  restLab
//
//  Created by maxim vingalov on 30.05.2018.
//  Copyright © 2018 maxim vingalov. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

protocol RegProtocol: AnyObject {
    func userCreated(success: Bool, reason: String?)
}

class RegPresenter: NSObject {
    
    weak var delegate: RegProtocol?
    var user: User?
    
    func createUser() {
        
        guard let userDictionary = user?.toJSON() else { return }
        
        ServerManager.shared.registerUser(userDictionary: userDictionary) { [weak self] (json) in
            
            print(json)
            if json["success"].boolValue == true {
                self?.authUser()
            } else {
                self?.delegate?.userCreated(success: false, reason: json["msg"].stringValue)
            }
            
        }
        
    }
    
    func authUser() {
        
        guard let userDictionary = user?.toJSON() else { return }
        
        ServerManager.shared.authUser(userDictionary: userDictionary) { [weak self] (json) in
            
            if (json["success"].boolValue == true) {
                
                if let token = json["token"].string {
                    
                    Constant.INNER_TOKEN = token
                    
                    KeychainWrapper.standard.set(token, forKey: "inner_token")
                    UserDefaults.standard.set(true, forKey: Constant.IS_AUTHORIZED)
                    
                    self?.delegate?.userCreated(success: true, reason: nil)
                }
                
            } else {
                //MARK: - TODO Handle errors
            }

        }
        
    }
    
    func setUserProperties(phone: String, password: String) {
        user = User(phone: phone, password: password)
        createUser()
    }
    
}
