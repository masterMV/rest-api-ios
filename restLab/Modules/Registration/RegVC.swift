//
//  AuthVC.swift
//  restLab
//
//  Created by maxim vingalov on 30.05.2018.
//  Copyright © 2018 maxim vingalov. All rights reserved.
//

import UIKit

class RegVC: BaseVC {
    
    fileprivate var presenter: RegPresenter!
    
    fileprivate var mainView: RegView { return self.view as! RegView }
    
    override func loadView() {
        self.view = RegView(controller: self)
    }
    
    init(with presenter: RegPresenter) {
        super.init()
        self.presenter = presenter
        self.presenter.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Регистрация"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func passToHomeVC() {
        
        let window = UIApplication.shared.keyWindow
        
        let presenter = HomePresenter()
        let initialVC = HomeVC(with: presenter)
        let navigation = UINavigationController(rootViewController: initialVC)
        
        window?.rootViewController = navigation
        
    }
    
    
    //MARK: - User Interaction Handlers
    
    @objc func acceptButtonDidTap() {
        
        mainView.endEditing(true)
        
        //validating fields
        if mainView.getAnotherPassword() != mainView.getPassword() {
            createAlert(title: "Ошибка", message: "Пароли должны совпадать")
        } else if mainView.getPhone() == "" || mainView.getPassword() == "" {
            createAlert(title: "Ошибка", message: "Номер телефона и пароль не должны быть пустыми")
        } else {
            presenter.setUserProperties(phone: mainView.getPhone(), password: mainView.getPassword())
        }
        
    }
    
    func createAlert(title: String, message: String?) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ОК", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
}

extension RegVC: RegProtocol {
    
    func userCreated(success: Bool, reason: String?) {
        
        if success == true {
            passToHomeVC()
        } else {
            createAlert(title: "Ошибка", message: reason)
        }
        
    }
    
}
