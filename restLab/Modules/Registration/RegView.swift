//
//  AuthView.swift
//  restLab
//
//  Created by maxim vingalov on 30.05.2018.
//  Copyright © 2018 maxim vingalov. All rights reserved.
//

import UIKit
import PinLayout

class RegView: BaseView {
    
    weak var controller: RegVC!
    
    fileprivate var phoneLabel: UILabel = {
        
        let label = UILabel()
        label.text = "Введите ваш номер телефона"
        label.numberOfLines = 1
        label.textColor = .white
        label.font = UIFont(name: "SFCompactText-Regular", size: 16)
        
        return label
        
    }()
    
    fileprivate var passLabel: UILabel = {
        
        let label = UILabel()
        label.text = "Введите пароль"
        label.numberOfLines = 1
        label.textColor = .white
        label.font = UIFont(name: "SFCompactText-Regular", size: 16)
        
        return label
        
    }()
    
    fileprivate var anotherPassLabel: UILabel = {
        
        let label = UILabel()
        label.text = "Повторите пароль"
        label.numberOfLines = 1
        label.textColor = .white
        label.font = UIFont(name: "SFCompactText-Regular", size: 16)
        
        return label
        
    }()
    
    
    fileprivate lazy var phoneTextField: UITextField = {
        
        let textField = UITextField()
        textField.keyboardType = .numberPad
        textField.borderStyle = UITextBorderStyle.roundedRect
        textField.placeholder = "Номер телефона"
        textField.tintColor = Constant.BASE_APP_COLOR
        textField.font = UIFont(name: "SFCompactText-Regular", size: 16)
        
        return textField
        
    }()
    
    fileprivate var passTextField: UITextField = {
        
        let textField = UITextField()
        textField.borderStyle = UITextBorderStyle.roundedRect
        textField.placeholder = "Пароль"
        textField.tintColor = Constant.BASE_APP_COLOR
        textField.font = UIFont(name: "SFCompactText-Regular", size: 16)
        
        return textField
        
    }()
    
    fileprivate var anotherPassTextField: UITextField = {
        
        let textField = UITextField()
        textField.borderStyle = UITextBorderStyle.roundedRect
        textField.placeholder = "Пароль"
        textField.tintColor = Constant.BASE_APP_COLOR
        textField.font = UIFont(name: "SFCompactText-Regular", size: 16)
        
        return textField
        
    }()
    
    fileprivate lazy var acceptButton: UIButton = {
        
        let button = UIButton(type: .system)
        button.setTitle("ПОДТВЕРДИТЬ", for: .normal)
        button.titleLabel?.font = UIFont(name: "SFCompactText-Regular", size: 16)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .orange
        button.addTarget(controller, action: #selector(RegVC.acceptButtonDidTap), for: .touchUpInside)
        button.layer.cornerRadius = 8.0

        return button
        
    }()
    
    
    init(controller: RegVC) {
        super.init(controller: controller)
        self.controller = controller
        
        self.backgroundColor = Constant.BASE_APP_COLOR
        
        addSubview(phoneLabel)
        addSubview(phoneTextField)
        addSubview(passLabel)
        addSubview(passTextField)
        addSubview(anotherPassLabel)
        addSubview(anotherPassTextField)
        addSubview(acceptButton)
        
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layoutUI()
    }
    
    fileprivate func layoutUI() {
        
        phoneLabel.pin.topLeft().right().height(20).margin(topLayoutGuide + 24, 24, 0)
        phoneTextField.pin.below(of: phoneLabel).left().right().height(40).margin(8, 16, 0)
        
        passLabel.pin.below(of: phoneTextField).left().right().height(20).margin(16, 24, 0)
        passTextField.pin.below(of: passLabel).left().right().height(40).margin(8, 16, 0)
        
        anotherPassLabel.pin.below(of: passTextField).left().right().height(20).margin(16, 24, 0)
        anotherPassTextField.pin.below(of: anotherPassLabel).left().right().height(40).margin(8, 16, 0)

        acceptButton.pin.below(of: anotherPassTextField).left().right().height(44).margin(24, 16, 0)
        
    }
    
    func getPhone() -> String {
        return self.phoneTextField.text ?? ""
    }
    
    func getPassword() -> String {
        return self.passTextField.text ?? ""
    }
    
    func getAnotherPassword() -> String {
        return self.anotherPassTextField.text ?? ""
    }

    
}

