//
//  AdminAdminHomeView.swift
//  restLab
//
//  Created by maxim vingalov on 31.05.2018.
//  Copyright © 2018 maxim vingalov. All rights reserved.
//

import UIKit

class AdminHomeView: BaseView {
    
    weak var controller: AdminHomeVC!
    
    lazy var refreshControl: UIRefreshControl = {
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(controller, action: #selector(AdminHomeVC.refreshData), for: .valueChanged)
        
        return refreshControl
        
    }()

    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.backgroundColor = .white
        tableView.dataSource = controller
        tableView.delegate = controller
        tableView.allowsSelection = true
        tableView.estimatedRowHeight = 60
        
        return tableView
    }()
    
    init(controller: AdminHomeVC) {
        super.init(controller: controller)
        self.controller = controller
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        addSubview(tableView)
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layoutUI()
    }
    
    fileprivate func layoutUI() {
        
        tableView.pin.all()
        
    }
    
    
}
