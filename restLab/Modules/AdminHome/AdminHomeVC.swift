//
//  AdminAdminHomeVC.swift
//  restLab
//
//  Created by maxim vingalov on 31.05.2018.
//  Copyright © 2018 maxim vingalov. All rights reserved.
//

import UIKit

class AdminHomeVC: BaseVC {
    
    fileprivate var presenter: AdminHomePresenter!
    
    fileprivate var mainView: AdminHomeView { return self.view as! AdminHomeView }
    
    override func loadView() {
        self.view = AdminHomeView(controller: self)
    }
    
    init(with presenter: AdminHomePresenter) {
        super.init()
        self.presenter = presenter
        self.presenter.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Админ"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "создать", style: .plain, target: self, action: #selector(showCreateBoardVC))

        NotificationCenter.default.addObserver(self, selector: #selector(boardsStateChanged), name: Notification.Name(Constant.BOARDS_STATE_CHANGED), object: nil)

    }
    
    @objc func boardsStateChanged() {
        presenter.loadOwnBoards()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @objc func refreshData() {
        
        presenter.loadOwnBoards()
        
    }

    
    @objc func showCreateBoardVC() {
        
        let boardPresenter = BoardPresenter()
        let boardVC = BoardVC(with: boardPresenter)
        
        self.navigationController?.pushViewController(boardVC, animated: true)

    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    
}

extension AdminHomeVC: AdminHomeProtocol {
    
    func boardsReceived(boards: [Board]) {
        mainView.refreshControl.endRefreshing()
        mainView.tableView.reloadData()
    }
    
    func boardDeleted() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constant.BOARDS_STATE_CHANGED), object: nil)
    }
    
}

extension AdminHomeVC: UITableViewDataSource, UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.boards?.count ?? 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "Cell"
        
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        
        if cell == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: cellIdentifier)
        }
        
        guard let boards = presenter.boards else { return cell! }
        
        let board = boards[indexPath.row]
        
        cell?.textLabel?.numberOfLines = 0
        cell?.textLabel?.text = board.title
        cell?.detailTextLabel?.text = board.desc
        cell?.accessoryType = .disclosureIndicator
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let boards = presenter.boards else { return }
        
        let board = boards[indexPath.row]
        
        let boardPresenter = BoardPresenter(with: board)
        let boardVC = BoardVC(with: boardPresenter)
        
        self.navigationController?.pushViewController(boardVC, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Мои доски объявлений"
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            
            presenter.removeData(index: indexPath.row)
            
        }
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Удалить"
    }

    
}
