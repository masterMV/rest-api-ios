//
//  AdminAdminHomePresenter.swift
//  restLab
//
//  Created by maxim vingalov on 31.05.2018.
//  Copyright © 2018 maxim vingalov. All rights reserved.
//

import UIKit

protocol AdminHomeProtocol: AnyObject {
    func boardsReceived(boards: [Board])
    func boardDeleted()
}


class AdminHomePresenter: NSObject {
    
    weak var delegate: AdminHomeProtocol?
    
    var boards: [Board]?
    
    override init() {
        super.init()
        
        loadOwnBoards()
        
    }
    
    func loadOwnBoards() {
        
        ServerManager.shared.getOwnBoards { [weak self] (json) in
            
            self?.boards = [Board]()
            
            for result in json["boards"].arrayValue {
                if let boardObj = result.dictionaryObject {
                    if let board = Board(JSON: boardObj) {
                        self?.boards?.append(board)
                    }
                }
            }
            
            if let boards = self?.boards {
                self?.delegate?.boardsReceived(boards: boards)
            }
            
        }

    }
    
    func removeData(index: Int) {
        
        guard var boards = boards else { return }
        
        let id = boards[index].id ?? ""
        
        boards.remove(at: index)
        
        ServerManager.shared.deleteBoard(id: id) { [weak self] (json) in
            
            if json["success"].boolValue == true {
                self?.delegate?.boardDeleted()
            }
            
        }
        
    }
    
    
}
