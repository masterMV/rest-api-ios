//
//  DetailsPresenter.swift
//  restLab
//
//  Created by maxim vingalov on 31.05.2018.
//  Copyright © 2018 maxim vingalov. All rights reserved.
//

import UIKit

class DetailsPresenter: NSObject {

    var board: Board!
    
    init(with board: Board) {
        super.init()
        
        self.board = board
        
    }
    
}
