//
//  DetailsView.swift
//  restLab
//
//  Created by maxim vingalov on 31.05.2018.
//  Copyright © 2018 maxim vingalov. All rights reserved.
//

import UIKit

class DetailsView: BaseView {

    weak var controller: DetailsVC!
    
    fileprivate var titleLabel: UILabel = {
        
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = .black
        label.textAlignment = .center
        label.font = UIFont(name: "SFCompactDisplay-Regular", size: 22)
        
        return label
        
    }()
    
    fileprivate var descLabel: UILabel = {
        
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = .black
        label.font = UIFont(name: "SFCompactText-Regular", size: 18)
        
        return label
        
    }()

    fileprivate var dateLabel: UILabel = {
        
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = .black
        label.textAlignment = .right
        label.font = UIFont(name: "SFCompactText-Regular", size: 16)
        
        return label
        
    }()
    
    fileprivate var priceLabel: UILabel = {
        
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = .black
        label.font = UIFont(name: "SFCompactText-Regular", size: 16)
        
        return label
        
    }()
    
    var imageView: UIImageView = {
        
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.layer.borderWidth = 1.0
        imageView.layer.borderColor = UIColor.lightGray.cgColor
        
        return imageView
        
    }()


    
    init(controller: DetailsVC) {
        super.init(controller: controller)
        self.controller = controller
        
        addSubview(titleLabel)
        addSubview(descLabel)
        addSubview(imageView)
        addSubview(priceLabel)
        addSubview(dateLabel)

    }
    
    func configure(title: String?, desc: String?, price: String?, date: String?) {
        
        titleLabel.text = title
        descLabel.text = desc
        priceLabel.text = price
        dateLabel.text = date
        
        layoutUI()
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layoutUI()
    }
    
    fileprivate func layoutUI() {
        
        titleLabel.pin.top(topLayoutGuide).left().right().margin(24, 16, 0).sizeToFit(.width)
        descLabel.pin.below(of: titleLabel).left().right().margin(16, 16, 0).sizeToFit(.width)
        imageView.pin.below(of: descLabel).left().right().height(bounds.width / 4 * 3).margin(16, 16, 0)
        priceLabel.pin.below(of: imageView).left().width(bounds.width / 2.0).height(20).margin(8, 16, 0)
        dateLabel.pin.below(of: imageView).right().width(bounds.width / 2.0).height(20).margin(8, 16, 0)

    }


}
