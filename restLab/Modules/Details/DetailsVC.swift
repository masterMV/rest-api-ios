//
//  DetailsVC.swift
//  restLab
//
//  Created by maxim vingalov on 31.05.2018.
//  Copyright © 2018 maxim vingalov. All rights reserved.
//

import UIKit
import AlamofireImage

class DetailsVC: BaseVC {

    fileprivate var presenter: DetailsPresenter!
    
    fileprivate var mainView: DetailsView { return self.view as! DetailsView }
    
    override func loadView() {
        self.view = DetailsView(controller: self)
    }
    
    init(with presenter: DetailsPresenter) {
        super.init()
        self.presenter = presenter
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mainView.configure(title: presenter.board.title, desc: presenter.board.desc, price: String(presenter.board.price ?? 0) + " руб.", date: "25.05.2017")
        
        if let url = URL(string: presenter.board.imageURL ?? "") {
            mainView.imageView.af_setImage(withURL: url)
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }

}
