//
//  BoardVC.swift
//  restLab
//
//  Created by maxim vingalov on 31.05.2018.
//  Copyright © 2018 maxim vingalov. All rights reserved.
//

import UIKit

class BoardVC: BaseVC {
    
    fileprivate var presenter: BoardPresenter!
    
    fileprivate var mainView: BoardView { return self.view as! BoardView }
    
    override func loadView() {
        self.view = BoardView(controller: self)
    }
    
    init(with presenter: BoardPresenter) {
        super.init()
        self.presenter = presenter
        self.presenter.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Редактирование"
        
        mainView.updateChangeButtonTitle(title: presenter.buttonTitle)
        mainView.configure(title: presenter.board?.title, desc: presenter.board?.desc, price: String(presenter.board?.price ?? 0), imageURL: presenter.board?.imageURL)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    //MARK: - User Interaction Handlers
    
    @objc func changeButtonDidTap() {
        
        mainView.endEditing(true)
        
        if presenter.state == "edit" {
            
            presenter.board?.title = mainView.getTitle()
            presenter.board?.desc = mainView.getDesc()
            presenter.board?.price = Int(mainView.getPrice() ?? "")
            presenter.board?.imageURL = mainView.getImageURL()
            
            presenter.update()

        } else {
            
            presenter.board = Board(title: mainView.getTitle(), desc: mainView.getDesc(), price: Int(mainView.getPrice() ?? ""), imageURL: mainView.getImageURL())
            
            presenter.create()
        }
        
        

    }
    
}

extension BoardVC: BoardProtocol {

    func boardUpdated() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constant.BOARDS_STATE_CHANGED), object: nil)
        _ = self.navigationController?.popViewController(animated: true)
    }
}

