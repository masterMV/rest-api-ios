//
//  BoardPresenter.swift
//  restLab
//
//  Created by maxim vingalov on 31.05.2018.
//  Copyright © 2018 maxim vingalov. All rights reserved.
//

import UIKit

protocol BoardProtocol: AnyObject {
    func boardUpdated()
}

class BoardPresenter: NSObject {

    weak var delegate: BoardProtocol?
    var board: Board?
    
    var state = ""
    var buttonTitle = ""
    
    init(with board: Board) {
        super.init()
        
        self.state = "edit"
        self.board = board
        self.buttonTitle = "Изменить"
    }
    
    override init() {
        super.init()
        state = "create"
        self.buttonTitle = "Создать"
    }
    
    func create() {
        
        guard let json = board?.toJSON() else { return }
        
        ServerManager.shared.createBoard(parameters: json) { [weak self] (json) in
            
            if json["success"].boolValue == true {
                self?.delegate?.boardUpdated()
            }
        }
        
    }

    
    func update() {
        
        guard let json = board?.toJSON() else { return }
        
        ServerManager.shared.updateBoard(id: board?.id ?? "", parameters: json) { [weak self] (json) in
            
            if json["success"].boolValue == true {
                self?.delegate?.boardUpdated()
            }
        }
        
    }

}
