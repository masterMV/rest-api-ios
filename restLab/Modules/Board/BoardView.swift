
//
//  BoardView.swift
//  restLab
//
//  Created by maxim vingalov on 31.05.2018.
//  Copyright © 2018 maxim vingalov. All rights reserved.
//

import UIKit

class BoardView: BaseView {
    
    weak var controller: BoardVC!
    
    fileprivate var titleLabel: UILabel = {
        
        let label = UILabel()
        label.text = "Введите название товара"
        label.numberOfLines = 1
        label.textColor = .lightGray
        label.font = UIFont(name: "SFCompactText-Regular", size: 14)
        
        return label
        
    }()
    
    fileprivate var descLabel: UILabel = {
        
        let label = UILabel()
        label.text = "Введите описание"
        label.numberOfLines = 1
        label.textColor = .lightGray
        label.font = UIFont(name: "SFCompactText-Regular", size: 14)
        
        return label
        
    }()
    
    fileprivate var priceLabel: UILabel = {
        
        let label = UILabel()
        label.text = "Введите цену"
        label.numberOfLines = 1
        label.textColor = .lightGray
        label.font = UIFont(name: "SFCompactText-Regular", size: 14)
        
        return label
        
    }()

    fileprivate var imageURLLabel: UILabel = {
        
        let label = UILabel()
        label.text = "Введите ссылку на фотографию"
        label.numberOfLines = 1
        label.textColor = .lightGray
        label.font = UIFont(name: "SFCompactText-Regular", size: 14)
        
        return label
        
    }()
    
    fileprivate lazy var titleTextField: UITextField = {
        
        let textField = UITextField()
        textField.borderStyle = UITextBorderStyle.roundedRect
        textField.font = UIFont(name: "SFCompactText-Regular", size: 18)
        
        return textField
        
    }()
    
    fileprivate lazy var descTextField: UITextField = {
        
        let textField = UITextField()
        textField.borderStyle = UITextBorderStyle.roundedRect
        textField.font = UIFont(name: "SFCompactText-Regular", size: 18)
        
        return textField
        
    }()
    
    fileprivate lazy var priceTextField: UITextField = {
        
        let textField = UITextField()
        textField.keyboardType = .numberPad
        textField.borderStyle = UITextBorderStyle.roundedRect
        textField.font = UIFont(name: "SFCompactText-Regular", size: 18)
        
        return textField
        
    }()

    fileprivate lazy var imageURLTextField: UITextField = {
        
        let textField = UITextField()
        textField.borderStyle = UITextBorderStyle.roundedRect
        textField.font = UIFont(name: "SFCompactText-Regular", size: 18)
        
        return textField
        
    }()


    fileprivate lazy var changeButton: UIButton = {
        
        let button = UIButton(type: .system)
        button.setTitle("", for: .normal)
        button.titleLabel?.font = UIFont(name: "SFCompactText-Regular", size: 18)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .orange
        button.addTarget(controller, action: #selector(BoardVC.changeButtonDidTap), for: .touchUpInside)
        button.layer.cornerRadius = 8.0
        
        return button
        
    }()
    
    
    init(controller: BoardVC) {
        super.init(controller: controller)
        self.controller = controller
        
        addSubview(titleLabel)
        addSubview(titleTextField)
        addSubview(descLabel)
        addSubview(descTextField)
        addSubview(priceLabel)
        addSubview(priceTextField)
        addSubview(imageURLLabel)
        addSubview(imageURLTextField)
        addSubview(changeButton)

    }
    
    func configure(title: String?, desc: String?, price: String?, imageURL: String?) {
        
        titleTextField.text = title
        descTextField.text = desc
        priceTextField.text = price
        imageURLTextField.text = imageURL
        
    }

    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layoutUI()
    }
    
    fileprivate func layoutUI() {
        
        titleLabel.pin.top(topLayoutGuide).left().right().margin(16, 16, 0).sizeToFit(.width)
        titleTextField.pin.below(of: titleLabel).left().right().height(44).margin(8, 16, 0)

        descLabel.pin.below(of: titleTextField).left().right().margin(16, 16, 0).sizeToFit(.width)
        descTextField.pin.below(of: descLabel).left().right().height(44).margin(8, 16, 0)

        priceLabel.pin.below(of: descTextField).left().right().margin(16, 16, 0).sizeToFit(.width)
        priceTextField.pin.below(of: priceLabel).left().right().height(44).margin(8, 16, 0)

        imageURLLabel.pin.below(of: priceTextField).left().right().margin(16, 16, 0).sizeToFit(.width)
        imageURLTextField.pin.below(of: imageURLLabel).left().right().height(44).margin(8, 16, 0)

        changeButton.pin.below(of: imageURLTextField).left().right().height(44).margin(16, 16, 0)
        
    }
    
    func updateChangeButtonTitle(title: String) {
        self.changeButton.setTitle(title, for: .normal)
    }
    
    func getTitle() -> String? {
        return titleTextField.text
    }
    
    func getDesc() -> String? {
        return descTextField.text
    }

    func getPrice() -> String? {
        return priceTextField.text
    }

    func getImageURL() -> String? {
        return imageURLTextField.text
    }

    
}

