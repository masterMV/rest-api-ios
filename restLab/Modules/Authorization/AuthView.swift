//
//  AuthView.swift
//  restLab
//
//  Created by maxim vingalov on 30.05.2018.
//  Copyright © 2018 maxim vingalov. All rights reserved.
//

import UIKit

class AuthView: BaseView {

    weak var controller: AuthVC!
    
    fileprivate var phoneLabel: UILabel = {
        
        let label = UILabel()
        label.text = "Введите ваш номер телефона"
        label.numberOfLines = 1
        label.textColor = .white
        label.font = UIFont(name: "SFCompactText-Authular", size: 16)
        
        return label
        
    }()
    
    fileprivate var passLabel: UILabel = {
        
        let label = UILabel()
        label.text = "Введите пароль"
        label.numberOfLines = 1
        label.textColor = .white
        label.font = UIFont(name: "SFCompactText-Authular", size: 16)
        
        return label
        
    }()
    
    fileprivate var descLabel: UILabel = {
        
        let label = UILabel()
        label.text = "Или"
        label.numberOfLines = 1
        label.textAlignment = .center
        label.textColor = .white
        label.font = UIFont(name: "SFCompactText-Authular", size: 16)
        
        return label
        
    }()
    
    fileprivate lazy var phoneTextField: UITextField = {
        
        let textField = UITextField()
        textField.keyboardType = .numberPad
        textField.borderStyle = UITextBorderStyle.roundedRect
        textField.placeholder = "Номер телефона"
        textField.tintColor = Constant.BASE_APP_COLOR
        textField.font = UIFont(name: "SFCompactText-Authular", size: 16)
        
        return textField
        
    }()
    
    fileprivate var passTextField: UITextField = {
        
        let textField = UITextField()
        textField.borderStyle = UITextBorderStyle.roundedRect
        textField.placeholder = "Пароль"
        textField.tintColor = Constant.BASE_APP_COLOR
        textField.font = UIFont(name: "SFCompactText-Authular", size: 16)
        
        return textField
        
    }()
    
    fileprivate var ipTextField: UITextField = {
        
        let textField = UITextField()
        textField.borderStyle = UITextBorderStyle.roundedRect
        textField.placeholder = "ip address"
        textField.tintColor = Constant.BASE_APP_COLOR
        textField.font = UIFont(name: "SFCompactText-Authular", size: 16)
        
        return textField
        
    }()

    
    fileprivate lazy var authButton: UIButton = {
        
        let button = UIButton(type: .system)
        button.setTitle("Войти", for: .normal)
        button.titleLabel?.font = UIFont(name: "SFCompactText-Authular", size: 18)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .orange
        button.addTarget(controller, action: #selector(AuthVC.authButtonDidTap), for: .touchUpInside)
        button.layer.cornerRadius = 8.0
        
        return button
        
    }()
    
    fileprivate lazy var registerButton: UIButton = {
        
        let button = UIButton(type: .system)
        button.setTitle("Зарегистрироваться", for: .normal)
        button.titleLabel?.font = UIFont(name: "SFCompactText-Authular", size: 18)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .orange
        button.addTarget(controller, action: #selector(AuthVC.registerButtonDidTap), for: .touchUpInside)
        button.layer.cornerRadius = 8.0

        return button
        
    }()

    
    
    init(controller: AuthVC) {
        super.init(controller: controller)
        self.controller = controller
        
        self.backgroundColor = Constant.BASE_APP_COLOR
        
        addSubview(phoneLabel)
        addSubview(phoneTextField)
        addSubview(passLabel)
        addSubview(passTextField)
        addSubview(authButton)
        addSubview(descLabel)
        addSubview(registerButton)
        addSubview(ipTextField)

    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layoutUI()
    }
    
    fileprivate func layoutUI() {
        
        phoneLabel.pin.topLeft().right().height(20).margin(topLayoutGuide + 24, 24, 0)
        phoneTextField.pin.below(of: phoneLabel).left().right().height(40).margin(8, 16, 0)
        
        passLabel.pin.below(of: phoneTextField).left().right().height(20).margin(16, 24, 0)
        passTextField.pin.below(of: passLabel).left().right().height(40).margin(8, 16, 0)
        
        authButton.pin.below(of: passTextField).left().right().height(44).margin(24, 16, 0)
        descLabel.pin.below(of: authButton).left().right().height(40)
        registerButton.pin.below(of: descLabel).left().right().height(44).margin(0, 16, 0)
        ipTextField.pin.below(of: registerButton).left().right().height(40).margin(16, 16, 0)

    }
    
    func getPhone() -> String {
        return self.phoneTextField.text ?? ""
    }
    
    func getPassword() -> String {
        return self.passTextField.text ?? ""
    }
    
    func getIpAddress() -> String {
        return self.ipTextField.text ?? ""
    }

    
}
