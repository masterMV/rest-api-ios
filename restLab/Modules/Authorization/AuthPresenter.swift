//
//  AuthPresenter.swift
//  restLab
//
//  Created by maxim vingalov on 30.05.2018.
//  Copyright © 2018 maxim vingalov. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

protocol AuthProtocol: AnyObject {
    func userAuthenticated(success: Bool, reason: String?)
}


class AuthPresenter: NSObject {

    weak var delegate: AuthProtocol?
    var user: User?

    func authUser() {
        
        guard let userDictionary = user?.toJSON() else { return }
        
        ServerManager.shared.authUser(userDictionary: userDictionary) { [weak self] (json) in
            
            print(json)
            if (json["success"].boolValue == true) {
                
                if let token = json["token"].string {
                    
                    Constant.INNER_TOKEN = token
                    
                    KeychainWrapper.standard.set(token, forKey: "inner_token")
                    UserDefaults.standard.set(true, forKey: Constant.IS_AUTHORIZED)
                    
                    self?.delegate?.userAuthenticated(success: true, reason: nil)
                }
                
            } else {
                self?.delegate?.userAuthenticated(success: false, reason: json["msg"].stringValue)
            }
            
        }
        
    }

    
    func setUserProperties(phone: String, password: String) {
        user = User(phone: phone, password: password)
        authUser()
    }
    
    
}
