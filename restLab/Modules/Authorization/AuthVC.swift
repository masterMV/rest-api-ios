//
//  AuthVC.swift
//  restLab
//
//  Created by maxim vingalov on 30.05.2018.
//  Copyright © 2018 maxim vingalov. All rights reserved.
//

import UIKit

class AuthVC: BaseVC {
    
    fileprivate var presenter: AuthPresenter!
    
    fileprivate var mainView: AuthView { return self.view as! AuthView }
    
    override func loadView() {
        self.view = AuthView(controller: self)
    }
    
    init(with presenter: AuthPresenter) {
        super.init()
        self.presenter = presenter
        self.presenter.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Авторизация"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func passToHomeVC() {
        
        let window = UIApplication.shared.keyWindow
        
        let homePresenter = HomePresenter()
        let initialVC = HomeVC(with: homePresenter)
        let navigation = UINavigationController(rootViewController: initialVC)
        
        window?.rootViewController = navigation
        
    }
    
    
    //MARK: - User Interaction Handlers
    
    @objc func authButtonDidTap() {
        
        mainView.endEditing(true)
        
        let ipAddress = mainView.getIpAddress()
        Constant.SERVER_URL = "http://\(ipAddress):8080/api/v1"
        
        //validating fields
        if mainView.getPhone() == "" || mainView.getPassword() == "" {
            createAlert(title: "Ошибка", message: "Номер телефона и пароль не должны быть пустыми")
        } else {
            presenter.setUserProperties(phone: mainView.getPhone(), password: mainView.getPassword())
        }
        
    }
    
    @objc func registerButtonDidTap() {
        
        mainView.endEditing(true)
        
        let presenter = RegPresenter()
        let regVC = RegVC(with: presenter)
        self.navigationController?.pushViewController(regVC, animated: true)
        
    }

    
    func createAlert(title: String, message: String?) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ОК", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
}

extension AuthVC: AuthProtocol {
    
    func userAuthenticated(success: Bool, reason: String?) {
        
        if success == true {
            passToHomeVC()
        } else {
            createAlert(title: "Ошибка", message: reason)
        }

    }
    
}
