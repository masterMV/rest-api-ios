//
//  ServerManager.swift
//  restLab
//
//  Created by maxim vingalov on 13.08.2018.
//  Copyright © 2018 maxim vingalov. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import ObjectMapper
import SwiftKeychainWrapper

typealias ServerResponse = (_ response: JSON) -> Void

class ServerManager: NSObject {
    
    private static let _instance = ServerManager()
    
    private var manager: SessionManager!
    
    static var shared: ServerManager {
        
        return _instance
    }
    
    override init() {
        super.init()
        
        manager = getConfigForManager()
        manager.delegate.taskWillPerformHTTPRedirection = { session, task, response, request in
            
            var redirectedRequest = request
            
            if let originalRequest = task.originalRequest, let headers = originalRequest.allHTTPHeaderFields, let authorizationHeaderValue = headers["Authorization"] {
                
                let mutableRequest = request as! NSMutableURLRequest
                mutableRequest.setValue(authorizationHeaderValue, forHTTPHeaderField: "Authorization")
                redirectedRequest = mutableRequest as URLRequest
                
            }
            
            return redirectedRequest
        }
    }
    
    func getConfigForManager() -> SessionManager {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 30 //seconds
        configuration.timeoutIntervalForResource = 30 //seconds
        
        return Alamofire.SessionManager(configuration: configuration)
    }
    
    //MARK: - Пользователи
    
    // MARK: Функция регистрации пользователя
    func registerUser(userDictionary: [String: Any], serverResponse: @escaping ServerResponse) -> Void {
        let URI = "/signup"
        
        manager.request(Constant.SERVER_URL + URI, method: .post, parameters: userDictionary, encoding: JSONEncoding.default)
            .responseJSON { [unowned self] response in
                
                self.handleGenericResponse(response: response, serverResponse: serverResponse)
        }
    }
    
    // MARK: Функция авторизации и получения внутреннего токена
    func authUser(userDictionary: [String: Any], serverResponse: @escaping ServerResponse) -> Void {
        let URI = "/login"
        
        manager.request(Constant.SERVER_URL + URI, method: .post, parameters: userDictionary)
            .responseJSON { [unowned self] response in
                
                self.handleGenericResponse(response: response, serverResponse: serverResponse)
        }
    }
    
    //MARK: - Доски
    
    // MARK: Функция получения всех досок объявлений
    func getBoards(serverResponse: @escaping ServerResponse) -> Void {
        let URI = "/boards"
        
        manager.request(Constant.SERVER_URL + URI)
            .responseJSON { [unowned self] response in
                
                self.handleGenericResponse(response: response, serverResponse: serverResponse)
        }
    }
    
    // MARK: Функция получения своих досок объявлений
    func getOwnBoards(serverResponse: @escaping ServerResponse) -> Void {
        let URI = "/boards/users"
        
        let headers = ["Authorization": Constant.INNER_TOKEN]
        
        manager.request(Constant.SERVER_URL + URI, headers: headers)
            .responseJSON { [unowned self] response in
                
                self.handleGenericResponse(response: response, serverResponse: serverResponse)
        }
    }
    
    // MARK: Функция создания доски объявления
    func createBoard(parameters: [String: Any], serverResponse: @escaping ServerResponse) -> Void {
        let URI = "/boards"
        
        let headers = ["Authorization": Constant.INNER_TOKEN]
        
        manager.request(Constant.SERVER_URL + URI, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { [unowned self] response in
                
                self.handleGenericResponse(response: response, serverResponse: serverResponse)
        }
    }
    
    // MARK: Функция обновления доски объявления
    func updateBoard(id: String, parameters: [String: Any], serverResponse: @escaping ServerResponse) -> Void {
        let URI = "/boards/" + id
        
        let headers = ["Authorization": Constant.INNER_TOKEN]
        
        manager.request(Constant.SERVER_URL + URI, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { [unowned self] response in
                
                self.handleGenericResponse(response: response, serverResponse: serverResponse)
        }
    }
    
    // MARK: Функция удаления своих досок объявлений
    func deleteBoard(id: String, serverResponse: @escaping ServerResponse) -> Void {
        let URI = "/boards/" + id
        
        let headers = ["Authorization": Constant.INNER_TOKEN]
        
        manager.request(Constant.SERVER_URL + URI, method: .delete, headers: headers)
            .responseJSON { [unowned self] response in
                
                self.handleGenericResponse(response: response, serverResponse: serverResponse)
        }
    }
    
    // MARK: Функция поиска объявления по названию и описанию
    func searchBoard(searchString: String, serverResponse: @escaping ServerResponse) -> Void {
        let URI = "/boards/search/text"
        
        let parameters = ["text": searchString]
        
        manager.request(Constant.SERVER_URL + URI, method: .get, parameters: parameters)
            .responseJSON { [unowned self] response in
                
                self.handleGenericResponse(response: response, serverResponse: serverResponse)
        }
    }
    
    
    
    func handleGenericResponse(response: DataResponse<Any>, serverResponse: @escaping ServerResponse) {
        
        if response.response?.statusCode == 401 {
            self.handleUnauthorizedRequest()
            return
        }
        
        switch response.result {
        case .success:
            if let value = response.result.value {
                
                let json = JSON(value)
                
                serverResponse(json)
                
            }
        case .failure(let error):
            
            print(response.response.debugDescription)
            print(response.request?.url?.absoluteString ?? "")
            
            self.handleFailureInternetConnection(error: error)
            
        }
        
    }
    
    func handleUnauthorizedRequest() {
        
        UserDefaults.standard.set(false, forKey: Constant.IS_AUTHORIZED)
        
        //        if let window = UIApplication.shared.keyWindow {
        //
        //            let signInVC = SignInVC()
        //            let navigation = UINavigationController(rootViewController: signInVC)
        //            window.rootViewController?.present(navigation, animated: true, completion: nil)
        //        }
        
        
        
    }
    
    func handleFailureInternetConnection(error: Error) {
        
        //NSURLErrorNotConnectedToInternet
        if error._code == -1009 {
            
            print("timeout exceeded")
            
            //            self.showAlert(message: "Время ответа от сервера превысило допустимую норму. Проверьте интернет-соединение")
            
            //            if let visibleVC = UIApplication.shared.keyWindow?.getVisibleVC() {
            //
            //                if let vc = visibleVC as? BaseVC {
            //
            //                    vc.checkInternetConnection()
            //
            //                }
            //            }
            
        }
        
    }
    
    
}
